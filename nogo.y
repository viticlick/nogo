%{
#include <cstdio>
#include <iostream>
#include <map>
#include <deque>
using namespace std;

extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;

extern int n_line; 
extern bool flex_error;

bool programa_correcto = true;

map<string,string> ambitoMap;

 
void yyerror(const char *s);
%}

%union {
	int ival;
	float fval;
	char *sval;	
	string* str_val;
}

%token <ival> INT
%token <fval> FLOAT
%token <str_val> IDENTIFICADOR

%token AND_TOKEN
%token ARRAY_TOKEN
%token BEGIN_TOKEN
%token CASE_TOKEN
%token CONSTANT_TOKEN
%token DECLARE_TOKEN
%token ELSE_TOKEN
%token END_TOKEN
%token EXIT_TOKEN
%token FOREACH_TOKEN
%token FOR_TOKEN
%token FUNCTION_TOKEN
%token IF_TOKEN
%token IN_TOKEN
%token IS_TOKEN
%token LOOP_TOKEN
%token MOD_TOKEN
%token NOT_TOKEN
%token NULL_TOKEN
%token OF_TOKEN
%token OR_TOKEN
%token OTHERS_TOKEN
%token OUT_TOKEN
%token PROCEDURE_TOKEN
%token RECORD_TOKEN
%token RETURN_TOKEN
%token REVERSE_TOKEN
%token THEN_TOKEN
%token TYPE_TOKEN
%token WHEN_TOKEN
%token WHILE_TOKEN
%token ERROR_TOKEN

%token INTEGER_TOKEN
%token REAL_TOKEN
%token BOOLEAN_TOKEN
%token CHARACTER_TOKEN

%token TRUE_TOKEN
%token FALSE_TOKEN

%token COMENTARIO
%token CARACTER
%token CADENA

%token ABRE_LLAVE
%token CIERRA_LLAVE
%token ABRE_PARENTESIS
%token CIERRA_PARENTESIS
%token ABRE_CORCHETE
%token CIERRA_CORCHETE
%token MULT
%token MAS
%token MENOS
%token COMA
%token PUNTO
%token DOS_PUNTOS
%token PUNTO_COMA
%token MENOR
%token IGUAL
%token MAYOR
%token PIPE
%token IGUAL_MAS
%token ASIG
%token EXP
%token DIV
%token PUNTOPUNTO
%token DISTINTO

%token MAYOR_IGUAL
%token MENOR_IGUAL
%token HASHTABLE_TOKEN

%start declaraciones

%%

declaraciones: 
  /** empty */
  | declaracion declaraciones {printf("BISON - (linea %d) - declaraciones => declaracion declaraciones \n" , n_line); } 
;

declaracion:
  declaracion_objeto {printf("BISON - (linea %d) - declaracion => declaracion_objeto \n" , n_line); }
  | declaracion_tipo {printf("BISON - (linea %d) - declaracion => declaracion_tipo \n" , n_line); }
  | declaracion_subprograma {printf("BISON - (linea %d) - declaracion => declaracion_subprograma \n" , n_line); } 
  | COMENTARIO
  | error 
  ;

declaracion_objeto: 
  identificadores DOS_PUNTOS constant tipo_escalar asignacion PUNTO_COMA
  {printf("BISON - (linea %d) - declaracion_objeto => identificadores : [constant] tipo_escalar [ asignacion ] ; \n" , n_line); }
  | identificadores DOS_PUNTOS tipo_compuesto PUNTO_COMA
  {printf("BISON - (linea %d) - declaracion_objeto => identificadores : tipo_compuesto ; \n" , n_line); }
  | identificadores DOS_PUNTOS nombre_de_tipo PUNTO_COMA
  ;
										
constant:
  /** empty */ 
  | CONSTANT_TOKEN {printf("BISON - (linea %d) - constant => constant_token \n" , n_line); }
  ;

tipo_escalar: 
  INTEGER_TOKEN 
  | REAL_TOKEN
  | BOOLEAN_TOKEN
  | CHARACTER_TOKEN
  ; 
								
asignacion:
  /** empty */
  | ASIG expresion {printf("BISON - (linea %d) - asignacion => := expresion \n" , n_line); }
  ;

identificadores: 
  IDENTIFICADOR COMA identificadores {printf("BISON - (linea %d) - identificadores => identificador , identificadores \n" , n_line); }
  | IDENTIFICADOR {printf("BISON - (linea %d) - identificadores => identificador \n" , n_line); }
  ;		

declaracion_tipo:	
  TYPE_TOKEN IDENTIFICADOR IS_TOKEN  tipo_compuesto PUNTO_COMA 
  ;

tipo_compuesto: 
  tipo_tablero
  | tipo_registro
  | tipo_hashtable
  ;
								
nombre_de_tipo:
  IDENTIFICADOR;

tipo_tablero: 
  ARRAY_TOKEN ABRE_PARENTESIS expresion  PUNTOPUNTO expresion CIERRA_PARENTESIS OF_TOKEN  especificacion_tipo 
  ;

especificacion_tipo:	
  tipo_escalar
  | nombre_de_tipo
  | tipo_compuesto
  ;

tipo_registro:	
  RECORD_TOKEN lista_componentes END_TOKEN RECORD_TOKEN 
  ;

lista_componentes: 
  componente lista_componentes
  | componente
  ;

componente:	
  identificadores DOS_PUNTOS especificacion_tipo PUNTO_COMA 
  ;

expresion:
  relacion continuacion_expresion
  | relacion
  ;

continuacion_expresion:	
  operador_logico relacion continuacion_expresion
  | operador_logico relacion
  ;

relacion:
  expresion_simple operador_relacional expresion_simple
  | expresion_simple
  ;
					
expresion_simple:	
  operador_unario termino continuacion_expresion_simple
  | operador_unario termino
  | termino continuacion_expresion_simple
  | termino
  ;

continuacion_expresion_simple: 
  operador_aditivo termino continuacion_expresion_simple
  | operador_aditivo termino 
  ;

termino:	
  factor continuacion_factor
  | factor
  ;
					
factor:
  primario EXP primario
  | primario
  ;					
					
					
continuacion_factor:
  operador_multiplicativo factor continuacion_factor
  | operador_multiplicativo factor
  ;

primario:
  literal
  | nombre
  | ABRE_PARENTESIS expresion CIERRA_PARENTESIS
  ;

literal:	
  INT
  | FLOAT
  | CARACTER
  | CADENA
  | TRUE_TOKEN
  | FALSE_TOKEN
  ;
					
operador_logico: 	AND_TOKEN
  | OR_TOKEN
  ;
									
operador_relacional:
  IGUAL
  | DISTINTO
  | MENOR
  | MENOR_IGUAL
  | MAYOR
  | MAYOR_IGUAL
  ;
										
operador_aditivo: 
  MAS
  | MENOS
  ;
									
operador_unario:
  MAS
  | MENOS
  | NOT_TOKEN
  ;
									
operador_multiplicativo:
  MULT
  | DIV
  | MOD_TOKEN
  ;
												
declaracion_subprograma: 
  especificacion_subprograma cuerpo_subprograma PUNTO_COMA 
  | especificacion_subprograma PUNTO_COMA
  ; 
						
especificacion_subprograma: 
  PROCEDURE_TOKEN IDENTIFICADOR ABRE_PARENTESIS parte_formal  CIERRA_PARENTESIS
  | PROCEDURE_TOKEN IDENTIFICADOR
  | FUNCTION_TOKEN IDENTIFICADOR ABRE_PARENTESIS parte_formal CIERRA_PARENTESIS RETURN_TOKEN especificacion_tipo
  | FUNCTION_TOKEN IDENTIFICADOR RETURN_TOKEN especificacion_tipo
  ;


parte_formal:
  /** empty */
  | declaracion_parametros
  ;

declaracion_parametros:
  declaracion_parametro PUNTO_COMA declaracion_parametros
  | declaracion_parametro
  ;

declaracion_parametro:
  identificadores DOS_PUNTOS modo especificacion_tipo
  ;

modo:
  /** empty */
  | IN_TOKEN OUT_TOKEN
  | IN_TOKEN
  ;

cuerpo_subprograma: 
  IS_TOKEN declaraciones BEGIN_TOKEN instrucciones END_TOKEN IDENTIFICADOR 
  | IS_TOKEN declaraciones BEGIN_TOKEN instrucciones END_TOKEN
  ;

instrucciones: 
  instruccion instrucciones
  | instruccion
  | error 
  ;

instruccion: 
  instruccion_simple 
  | instruccion_compuesta
  | COMENTARIO
  ;

instruccion_simple:
  instruccion_vacia
  | instruccion_asignacion
  | instruccion_exit
  | instruccion_return
  | llamada_procedure
  ;

instruccion_vacia: NULL_TOKEN PUNTO_COMA;

instruccion_asignacion: nombre ASIG expresion PUNTO_COMA ;

instruccion_return: RETURN_TOKEN expresion PUNTO_COMA ;


instruccion_compuesta: 
  instruccion_if
  | instruccion_case
  | instruccion_loop
  | instruccion_foreach
  ;

instruccion_foreach:
  FOREACH_TOKEN IDENTIFICADOR IN_TOKEN IDENTIFICADOR bucle_base PUNTO_COMA
  ;

instruccion_if:
  IF_TOKEN expresion THEN_TOKEN instrucciones ELSE_TOKEN instrucciones END_TOKEN IF_TOKEN PUNTO_COMA 
  | IF_TOKEN expresion THEN_TOKEN instrucciones END_TOKEN IF_TOKEN PUNTO_COMA
  ;

instruccion_case: 
  CASE_TOKEN expresion IS_TOKEN instrucciones_when END_TOKEN CASE_TOKEN PUNTO_COMA 
  ;

instrucciones_when:
  /** empty */
  | WHEN_TOKEN entrada mas_entradas_when IGUAL_MAS instrucciones instrucciones_when 
  ;
 
mas_entradas_when:
  /** emtpy */
  | PIPE entrada instrucciones_when
  ;

entrada:
  expresion PUNTOPUNTO expresion
  | expresion
  | OTHERS_TOKEN
  ;

instruccion_loop:
  identificador_bucle clausula_iteracion bucle_base identificador_bucle PUNTO_COMA 
  ;

identificador_bucle: 
	/** empty */
	| IDENTIFICADOR DOS_PUNTOS 
	;

clausula_iteracion:
  FOR_TOKEN indice_bucle IN_TOKEN REVERSE_TOKEN expresion PUNTOPUNTO expresion
  | FOR_TOKEN indice_bucle IN_TOKEN expresion PUNTOPUNTO expresion
  | WHILE_TOKEN expresion 
  ;

indice_bucle: IDENTIFICADOR ;

bucle_base:
  LOOP_TOKEN instrucciones END_TOKEN LOOP_TOKEN 
  ;

llamada_procedure: 
	IDENTIFICADOR parametros_reales PUNTO_COMA 
	| IDENTIFICADOR PUNTO_COMA;

parametros_reales: ABRE_PARENTESIS expresion mas_parametros_reales CIERRA_PARENTESIS ;

mas_parametros_reales: 
  /** empty */
  | COMA expresion mas_parametros_reales
  ;

instruccion_exit:
  EXIT_TOKEN identificador_bucle WHEN_TOKEN expresion PUNTO_COMA 
  | EXIT_TOKEN identificador_bucle PUNTO_COMA
  ;

nombre: 
  llamada_funcion
  | componente_indexado
  | componente_hash
  | componente_seleccionado
  | IDENTIFICADOR
  ;

componente_hash:
  nombre ABRE_LLAVE expresion CIERRA_LLAVE
  ;

componente_indexado:
  componente_indexado_reducido ABRE_PARENTESIS expresion CIERRA_PARENTESIS
  ;

componente_indexado_reducido:
  llamada_funcion
  | componente_indexado
  | componente_seleccionado
  ;

componente_seleccionado:
  nombre PUNTO IDENTIFICADOR
  ;

llamada_funcion: 
  IDENTIFICADOR parametros_reales
  | IDENTIFICADOR ABRE_PARENTESIS CIERRA_PARENTESIS
  ;

tipo_hashtable: 
  HASHTABLE_TOKEN  OF_TOKEN MENOR especificacion_tipo  COMA especificacion_tipo MAYOR
  ;


%%

int main(int argc, char** argv) {

  if(argc < 2){
    fprintf(stderr, "./parser <file> \n");
    return(-1);
  }

  yyin = fopen(argv[1], "r");
  if(yyin == 0){
    fprintf(stderr, "No se puee abrir el archivo (%s)\n", argv[1]);
    return(-2);
  }


  if(yyparse()!=1 && programa_correcto && !  flex_error ){
    printf("Todo ha ido bien\n");
    fclose(yyin);
    return 0;
  }else{
    printf("El programa ha finalizado con errores\n");
    return(-2);
  }

  return -3;
	
	/**
	do {
		yyparse();
	} while (!feof(yyin));
	*/
}

void yyerror(const char *s) {
	fprintf(stderr, "ERROR: %s en la línea %d\n", s, n_line);
	programa_correcto = false;
}
