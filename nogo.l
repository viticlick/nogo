%{
#include <cstdio>
#include <iostream>
#include <string>
using namespace std;
#define YY_DECL extern "C" int yylex()

#include "nogo.tab.h"  

int n_line = 1;
bool flex_error = false;

%}

A [aA]
B [bB]
C [cC]
D [dD]
E [eE]
F [fF]
G [gG]
H [hH]
I [iI]
J [jJ]
K [kK]
L [lL]
M [mM]
N [nN]
O [oO]
P [pP]
Q [qQ]
R [rR]
S [sS]
T [tT]
U [uU]
V [vV]
W [wW]
X [xX]
Y [yY]
Z [zZ]

DIGITO [0123456789]+


%%

{A}{N}{D} 						{printf("FLEX - (linea %d) - PALABRA RESERVADA : AND - %s\n", n_line ,  yytext);return AND_TOKEN;}
{A}{R}{R}{A}{Y}				{printf("FLEX - (linea %d) - PALABRA RESERVADA : ARRAY - %s\n", n_line ,  yytext);return ARRAY_TOKEN;}
{B}{E}{G}{I}{N}				{printf("FLEX - (linea %d) - PALABRA RESERVADA : BEGIN - %s\n", n_line ,  yytext);return BEGIN_TOKEN;}
{C}{A}{S}{E}					{printf("FLEX - (linea %d) - PALABRA RESERVADA : CASE - %s\n", n_line ,  yytext);return CASE_TOKEN;}
{C}{O}{N}{S}{T}{A}{N}{T}	{printf("FLEX - (linea %d) - PALABRA RESERVADA : CONSTANT - %s\n", n_line ,  yytext);return CONSTANT_TOKEN;}
{D}{E}{C}{L}{A}{R}{E}		{printf("FLEX - (linea %d) - PALABRA RESERVADA : DECALRE - %s\n", n_line ,  yytext);return DECLARE_TOKEN;}
{E}{L}{S}{E}					{printf("FLEX - (linea %d) - PALABRA RESERVADA : ELSE - %s\n", n_line ,  yytext);return ELSE_TOKEN;}
{E}{N}{D}						{printf("FLEX - (linea %d) - PALABRA RESERVADA : END - %s\n", n_line ,  yytext);return END_TOKEN;}
{E}{X}{I}{T}					{printf("FLEX - (linea %d) - PALABRA RESERVADA : EXIT - %s\n", n_line ,  yytext);return EXIT_TOKEN;}
{F}{O}{R}{E}{A}{C}{H}		{printf("FLEX - (linea %d) - PALABRA RESERVADA : FOREACH - %s\n" , n_line , yytext); return FOREACH_TOKEN; }
{F}{O}{R}						{printf("FLEX - (linea %d) - PALABRA RESERVADA : FOR - %s\n", n_line ,  yytext);return FOR_TOKEN;}
{F}{U}{N}{C}{T}{I}{O}{N}	{printf("FLEX - (linea %d) - PALABRA RESERVADA : FUNCTION - %s\n", n_line ,  yytext);return FUNCTION_TOKEN;}
{I}{F}							{printf("FLEX - (linea %d) - PALABRA RESERVADA : IF - %s\n", n_line ,  yytext);return IF_TOKEN;}
{I}{N}							{printf("FLEX - (linea %d) - PALABRA RESERVADA : IN - %s\n", n_line ,  yytext);return IN_TOKEN;}
{I}{S}							{printf("FLEX - (linea %d) - PALABRA RESERVADA : IS - %s\n", n_line ,  yytext);return IS_TOKEN;}
{L}{O}{O}{P}					{printf("FLEX - (linea %d) - PALABRA RESERVADA : LOOP - %s\n", n_line ,  yytext);return LOOP_TOKEN;}
{M}{O}{D}						{printf("FLEX - (linea %d) - PALABRA RESERVADA : MOD - %s\n", n_line ,  yytext);return MOD_TOKEN;}
{N}{O}{T}						{printf("FLEX - (linea %d) - PALABRA RESERVADA : NOT - %s\n", n_line ,  yytext);return NOT_TOKEN;}
{N}{U}{L}{L}					{printf("FLEX - (linea %d) - PALABRA RESERVADA : NULL - %s\n", n_line ,  yytext);return NULL_TOKEN;}
{O}{F}							{printf("FLEX - (linea %d) - PALABRA RESERVADA : OF - %s\n", n_line ,  yytext);return OF_TOKEN;}
{O}{R}							{printf("FLEX - (linea %d) - PALABRA RESERVADA : OR - %s\n", n_line ,  yytext);return OR_TOKEN;}
{O}{T}{H}{E}{R}{S}			{printf("FLEX - (linea %d) - PALABRA RESERVADA : OTHERS - %s\n", n_line ,  yytext);return OTHERS_TOKEN;}
{O}{U}{T}						{printf("FLEX - (linea %d) - PALABRA RESERVADA : OUT - %s\n", n_line ,  yytext);return OUT_TOKEN;}
{P}{R}{O}{C}{E}{D}{U}{R}{E} {printf("FLEX - (linea %d) - PALABRA RESERVADA : PROCEDURE - %s\n", n_line ,  yytext);return PROCEDURE_TOKEN;}
{R}{E}{C}{O}{R}{D}			{printf("FLEX - (linea %d) - PALABRA RESERVADA : RECORD - %s\n", n_line ,  yytext);return RECORD_TOKEN;}
{R}{E}{T}{U}{R}{N}			{printf("FLEX - (linea %d) - PALABRA RESERVADA : RETURN - %s\n", n_line ,  yytext);return RETURN_TOKEN;}
{R}{E}{V}{E}{R}{S}{E}		{printf("FLEX - (linea %d) - PALABRA RESERVADA : REVERSE - %s\n", n_line ,  yytext);return REVERSE_TOKEN;}
{T}{H}{E}{N}					{printf("FLEX - (linea %d) - PALABRA RESERVADA : THEN - %s\n", n_line ,  yytext);return THEN_TOKEN;}
{T}{Y}{P}{E}					{printf("FLEX - (linea %d) - PALABRA RESERVADA : TYPE - %s\n", n_line ,  yytext);return TYPE_TOKEN;}
{W}{H}{E}{N}					{printf("FLEX - (linea %d) - PALABRA RESERVADA : WHEN - %s\n", n_line ,  yytext);return WHEN_TOKEN;}
{W}{H}{I}{L}{E}				{printf("FLEX - (linea %d) - PALABRA RESERVADA : WHILE - %s\n", n_line ,  yytext);return WHILE_TOKEN;}

{I}{N}{T}{E}{G}{E}{R}		{printf("FLEX - (linea %d) - PALABRA RESERVADA : INTEGER - %s\n", n_line ,  yytext);return INTEGER_TOKEN;}
{R}{E}{A}{L}					{printf("FLEX - (linea %d) - PALABRA RESERVADA : REAL - %s\n", n_line ,  yytext);return REAL_TOKEN;}
{B}{O}{O}{L}{E}{A}{N}		{printf("FLEX - (linea %d) - PALABRA RESERVADA : BOOLEAN - %s\n", n_line ,  yytext);return BOOLEAN_TOKEN;}
{C}{H}{A}{R}{A}{C}{T}{E}{R} {printf("FLEX - (linea %d) - PALABRA RESERVADA : CHARACTER - %s\n", n_line ,  yytext);return CHARACTER_TOKEN;}

{T}{R}{U}{E}					{printf("FLEX - (linea %d) - PALABRA RESERVADA : TRUE - %s\n", n_line ,  yytext);return TRUE_TOKEN;}
{F}{A}{L}{S}{E}				{printf("FLEX - (linea %d) - PALABRA RESERVADA : FALSE - %s\n", n_line ,  yytext);return FALSE_TOKEN;}

{H}{A}{S}{H}{T}{A}{B}{L}{E} {printf("FLEX - (linea %d) - PALABRA RESERVADA : HASHTABLE - %s\n",n_line , yytext); return HASHTABLE_TOKEN;}

"--".* {printf("FLEX - (linea %d) - COMENTARIO - %s\n", n_line ,  yytext); return COMENTARIO ;}
'.'						{printf("FLEX - (linea %d) - CARACTER - %s\n", n_line ,  yytext); return CARACTER;}


\".*\"				{
								int count = 0;
								int index = 0;
								while( yytext[index] != '\0' ){
									if( yytext[index++] == '\"') count++;
								}
								if( count % 2 == 0){
									printf("FLEX - (linea %d) - CADENA - %s\n", n_line ,  yytext);
									return CADENA;
								}else{
									fprintf(stderr, "FLEX - (linea %d) - ERROR: CADENA MAL FORMADA\n" , n_line);
								}
								
							}

[a-zA-Z_][a-zA-Z0-9_]* {
													printf("FLEX - (linea %d) - IDENTIFICADOR - %s\n", n_line ,  yytext);
													yylval.str_val = new string(yytext);
 													return IDENTIFICADOR ;
 											 }
{DIGITO}[\.]{DIGITO}{E}[\+]?{DIGITO} {	
																				printf("FLEX - (linea %d) - FLOAT - %s\n", n_line ,  yytext);
																				yylval.fval = atof(yytext); 
																				return FLOAT; 
																			}
{DIGITO}[\.]{DIGITO}  {	
												printf("FLEX - (linea %d) - FLOAT - %s\n", n_line ,  yytext); 
												yylval.fval = atof(yytext);
												return FLOAT; 
											}
{DIGITO}							{ 
												printf("FLEX - (linea %d) - INT - %s\n", n_line ,  yytext); 
												yylval.ival = atoi(yytext);
												return INT ;
											}


"{"     {printf("FLEX - (linea %d) - ABRE_LLAVE - %s\n", n_line ,  yytext); return ABRE_LLAVE;}
"}"     {printf("FLEX - (linea %d) - CIERRA_LLAVE - %s\n", n_line ,  yytext); return CIERRA_LLAVE;}
"("     {printf("FLEX - (linea %d) - ABRE_PARENTESIS - %s\n", n_line ,  yytext); return ABRE_PARENTESIS;}
")"     {printf("FLEX - (linea %d) - CIERRA_PARENTESIS - %s\n", n_line ,  yytext); return CIERRA_PARENTESIS;}
"["     {printf("FLEX - (linea %d) - ABRE_CORCHETE - %s\n", n_line ,  yytext); return ABRE_CORCHETE;}
"]"     {printf("FLEX - (linea %d) - CIERRA_CORCHETE - %s\n", n_line ,  yytext); return CIERRA_CORCHETE;}
"*"     {printf("FLEX - (linea %d) - MULT - %s\n", n_line ,  yytext); return MULT;}
"+"     {printf("FLEX - (linea %d) - MAS - %s\n", n_line ,  yytext); return MAS;}
"-"     {printf("FLEX - (linea %d) - MENOS - %s\n", n_line ,  yytext); return MENOS;}
","     {printf("FLEX - (linea %d) - COMA - %s\n", n_line ,  yytext); return COMA;}
"."     {printf("FLEX - (linea %d) - PUNTO - %s\n", n_line ,  yytext); return PUNTO;}
":"     {printf("FLEX - (linea %d) - DOS_PUNTOS - %s\n", n_line ,  yytext); return DOS_PUNTOS;}
";"     {printf("FLEX - (linea %d) - PUNTO_COMA - %s\n", n_line ,  yytext); return PUNTO_COMA;}
"<"     {printf("FLEX - (linea %d) - MENOR - %s\n", n_line ,  yytext); return MENOR;}
"="     {printf("FLEX - (linea %d) - IGUAL - %s\n", n_line ,  yytext); return IGUAL;}
">"     {printf("FLEX - (linea %d) - MAYOR - %s\n", n_line ,  yytext); return MAYOR;}
"|"     {printf("FLEX - (linea %d) - PIPE - %s\n", n_line ,  yytext); return PIPE;}
"=>"     {printf("FLEX - (linea %d) - IGUAL_MAS - %s\n", n_line ,  yytext); return IGUAL_MAS;}
":="     {printf("FLEX - (linea %d) - ASIG - %s\n", n_line ,  yytext); return ASIG;}
"**"     {printf("FLEX - (linea %d) - DEXP - %s\n", n_line ,  yytext); return EXP;}
"/"     {printf("FLEX - (linea %d) - DIV - %s\n", n_line ,  yytext); return DIV;}
".."     {printf("FLEX - (linea %d) - PUNTOPUNTO - %s\n", n_line ,  yytext); return PUNTOPUNTO;}
"/="     {printf("FLEX - (linea %d) - DISTINTO - %s\n", n_line ,  yytext); return DISTINTO;}
">="     {printf("FLEX - (linea %d) - MAYOR_IGUAL - %s\n", n_line ,  yytext); return MAYOR_IGUAL;}
"<="     {printf("FLEX - (linea %d) - MENOR_IGUAL - %s\n", n_line ,  yytext); return MENOR_IGUAL;}


"\n"		{printf("FLEX - (linea %d) - SALTO DE LINEA \n", n_line ); n_line++ ;}
[ \t]+	;/* ignore whitespace */

.				{fprintf(stderr,"FLEX - (linea %d) - CARACTER NO IDENTIFICADO - %s\n", n_line , yytext) ; flex_error = true ;}
{DIGITO}[a-zA-Z_]+ {fprintf(stderr,"FLEX - (linea %d) - CARACTERES NO IDENTIFICADOS - %s\n", n_line , yytext) ;  flex_error = true ;}

%%
