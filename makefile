clean: parser
	rm nogo.tab.*
	rm lex.yy.c
parser: lex.yy.c nogo.tab.c nogo.tab.h
	g++ nogo.tab.c lex.yy.c -lfl -o parser
lex.yy.c: nogo.l nogo.tab.h
	flex nogo.l
nogo.tab.c nogo.tab.h: nogo.y
	bison -d nogo.y

